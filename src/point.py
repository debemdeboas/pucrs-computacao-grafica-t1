# ************************************************
#   Ponto.py
#   Define a classe Ponto
#   Autor: Márcio Sarroglia Pinho
#       pinho@pucrs.br
# ************************************************

import math
from typing import Tuple
from src.constants import Numeric

""" Classe Ponto """
class Point:
    def __init__(self, x: Numeric = 0, y: Numeric = 0, z: Numeric = 0):
        self.x = x
        self.y = y
        self.z = z

    """ Imprime os valores de cada eixo do ponto """
    # Faz a impressao usando sobrecarga de funcao
    # https://www.educative.io/edpresso/what-is-method-overloading-in-python
    def print(self, msg = None):
        if msg is not None:
            print(msg, self.x, self.y, self.z)
        else:
            print(self.x, self.y, self.z)

    """ Define os valores dos eixos do ponto """
    def set(self, x: Numeric, y: Numeric, z: Numeric = 0):
        self.x = x
        self.y = y
        self.z = z

# Definicao de operadores
# https://www.programiz.com/python-programming/operator-overloading
    def __add__(self, other):
        if not isinstance(other, self.__class__):
            raise TypeError('Unsupported operand type(s) for \'{}\' and \'{}\'.'.format(self.__class__, type(other)))
        x = self.x + other.x
        y = self.y + other.y
        return Point(x, y)

    def __sub__(self, other):
        if not isinstance(other, self.__class__):
            raise TypeError('Unsupported operand type(s) for \'{}\' and \'{}\'.'.format(self.__class__, type(other)))
        x = self.x - other.x
        y = self.y - other.y
        return Point(x, y)

    def __mul__(self, other: Numeric):
        x = self.x * other
        y = self.y * other
        return Point(x, y)

    def rotate_z(self, angle: Numeric):
        angle_in_rad = angle * math.pi / 180.0
        xr = self.x * math.cos(angle_in_rad) - self.y * math.sin(angle_in_rad)
        yr = self.x * math.sin(angle_in_rad) + self.y * math.cos(angle_in_rad)
        self.x = xr
        self.y = yr

    def rotate_y(self, angle: Numeric):
        angle_in_rad = angle * math.pi / 180.0
        xr = self.x * math.cos(angle_in_rad) + self.z * math.sin(angle_in_rad)
        zr = -self.x * math.sin(angle_in_rad) + self.z * math.cos(angle_in_rad)
        self.x = xr
        self.z = zr

    def rotate_x(self, angle: Numeric):
        angle_in_rad = angle * math.pi / 180.0
        yr = self.y * math.cos(angle_in_rad) - self.z * math.sin(angle_in_rad)
        zr = self.y * math.sin(angle_in_rad) + self.z * math.cos(angle_in_rad)
        self.y = yr
        self.z = zr

# ********************************************************************** */
#                                                                        */
#  Calcula a interseccao entre 2 retas (no plano "XY" Z = 0)             */
#                                                                        */
# k : ponto inicial da reta 1                                            */
# l : ponto final da reta 1                                              */
# m : ponto inicial da reta 2                                            */
# n : ponto final da reta 2                                              */
#
# Retorna:
# 0, se não houver interseccao ou 1, caso haja                           */
# int, valor do parâmetro no ponto de interseção (sobre a reta KL)       */
# int, valor do parâmetro no ponto de interseção (sobre a reta MN)       */
#                                                                        */
# ********************************************************************** */
def intersect_2d(k: Point, l: Point, m: Point, n: Point) -> Tuple[int, float | None, float | None]:
    det = (n.x - m.x) * (l.y - k.y) - (n.y - m.y) * (l.x - k.x)

    if det == 0.0:
        return 0, None, None  # não há intersecção

    s = ((n.x - m.x) * (m.y - k.y) - (n.y - m.y) * (m.x - k.x)) / det
    t = ((l.x - k.x) * (m.y - k.y) - (l.y - k.y) * (m.x - k.x)) / det

    return 1, s, t  # há intersecção

# **********************************************************************
# HaInterseccao(k: Ponto, l: Ponto, m: Ponto, n: Ponto)
# Detecta interseccao entre os pontos
#
# **********************************************************************
def has_intersection(k: Point, l: Point, m: Point, n: Point) -> bool:
    ret, s, t = intersect_2d(k,  l,  m,  n)

    if not ret or not s or not t:
        return False

    return s >= 0.0 and s <= 1.0 and t >= 0.0 and t <= 1.0
