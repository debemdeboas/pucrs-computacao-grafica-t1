# ************************************************
#   Poligonos.py
#   Define a classe Polygon
#   Autor: Márcio Sarroglia Pinho
#       pinho@pucrs.br
# ************************************************
from typing import List
from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *
from src.point import *
from src.constants import *
import copy
import random


Vector = Tuple[Point, Point]


class Polygon:
    @staticmethod
    def cross_product(a: Vector, query_point: Point) -> Side:
        cross = (a[1].x - a[0].x) * (query_point.y - a[0].y) - (a[1].y - a[0].y) * (query_point.x - a[0].x)
        if cross < 0:
            return Side.Left
        elif cross > 0:
            return Side.Right
        else:
            return Side.Atop

    def __init__(self):
        self.vertices: List[Point] = []  # atributo do objeto

    def get_vertices_amount(self):
        return len(self.vertices)

    def insert_vertex(self, x: float, y: float, z: float):
        self.vertices += [Point(x, y, z)]

    def get_vertex(self, i):
        temp = copy.deepcopy(self.vertices[i])
        return temp
        # return self.Vertices[i]

    def draw(self):
        #print ("Desenha Poligono - Tamanho:", len(self.Vertices))
        glBegin(GL_LINE_LOOP)
        for v in self.vertices:
            glVertex3f(v.x, v.y, v.z)
        glEnd()

    def draw_vertices(self):
        glBegin(GL_POINTS)
        for v in self.vertices:
            glVertex3f(v.x, v.y, v.z)
        glEnd()

    def print_vertices(self):
        for x in self.vertices:
            x.print()

    def get_limits(self):
        min = copy.deepcopy(self.vertices[0])
        max = copy.deepcopy(self.vertices[0])

        for v in self.vertices:
            if v.x > max.x:
                max.x = v.x
            if v.y > max.y:
                max.y = v.y
            if v.z > max.z:
                max.z = v.z
            if v.x < min.x:
                min.x = v.x
            if v.y < min.y:
                min.y = v.y
            if v.z < min.z:
                min.z = v.z
        # print("getLimits")
        # Min.imprime()
        # Max.imprime()
        return min, max
# def setColor()
# ***********************************************************************************
# LePontosDeArquivo(Nome):
#  Realiza a leitura de uam arquivo com as coordenadas do polígono
# ***********************************************************************************
    def read_points_from_file(self, filename):
        point = Point()
        infile = open(filename)
        line = infile.readline()
        n_of_lines = int(line)
        for line in infile:
            words = line.split()  # Separa as palavras na linha
            x = float(words[0])
            y = float(words[1])
            self.insert_vertex(x, y, 0)
            # Mapa.insereVertice(*map(float,line.split))
        infile.close()

        #print ("Após leitura do arquivo:")
        # Min.imprime()
        # Max.imprime()
        return self.get_limits()

    def get_edge(self, n):
        p1 = self.vertices[n]
        n1 = (n+1) % self.get_vertices_amount()
        p2 = self.vertices[n1]
        return p1, p2

    def draw_edge(self, n):
        glBegin(GL_LINES)
        glVertex3f(self.vertices[n].x, self.vertices[n].y, self.vertices[n].z)
        n1 = (n+1) % self.get_vertices_amount()
        glVertex3f(self.vertices[n1].x,
                   self.vertices[n1].y, self.vertices[n1].z)
        glEnd()

    def modify_edge(self, i: int, p: Point):
        self.vertices[i] = p


class RecursivePolygon(Polygon):
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.children: List[RecursivePolygon] = []
        self.color = (random.randint(70, 255)/255,
                      random.randint(70, 255)/255,
                      random.randint(70, 255)/255)

    def subdivide(self) -> None:
        _min, _max = self.get_limits()
        mid_x = abs((_min.x + _max.x) / 2)
        mid_y = abs((_min.y + _max.y) / 2)
        _mid = Point(mid_x, mid_y, 0)

        child = RecursivePolygon()
        child.insert_vertex(_min.x, _min.y, 0)
        child.insert_vertex(_mid.x, _min.y, 0)
        child.insert_vertex(_mid.x, _mid.y, 0)
        child.insert_vertex(_min.x, _mid.y, 0)
        self.children.append(child)

        child = RecursivePolygon()
        child.insert_vertex(_mid.x, _min.y, 0)
        child.insert_vertex(_max.x, _min.y, 0)
        child.insert_vertex(_max.x, _mid.y, 0)
        child.insert_vertex(_mid.x, _mid.y, 0)
        self.children.append(child)

        child = RecursivePolygon()
        child.insert_vertex(_min.x, _mid.y, 0)
        child.insert_vertex(_mid.x, _mid.y, 0)
        child.insert_vertex(_mid.x, _max.y, 0)
        child.insert_vertex(_min.x, _max.y, 0)
        self.children.append(child)

        child = RecursivePolygon()
        child.insert_vertex(_mid.x, _mid.y, 0)
        child.insert_vertex(_max.x, _mid.y, 0)
        child.insert_vertex(_max.x, _max.y, 0)
        child.insert_vertex(_mid.x, _max.y, 0)
        self.children.append(child)

    def draw(self):
        glColor4f(*self.color, 0.1)
        glEnable(GL_BLEND)
        glBegin(GL_QUADS)
        for v in self.vertices:
            glVertex3f(v.x, v.y, v.z)
        glEnd()
        glDisable(GL_BLEND)

        glColor3f(*self.color)
        super().draw()
        for child in self.children:
            child.draw()

    def vector_in_quadtree(self, vector: Vector):
        children = []
        for child in self.children:
            if node := child.vector_in_quadtree(vector):
                children += node
        if children:
            return children

        for i, p in enumerate(self.vertices):
            next_i = i + 1 if i + 1 < len(self.vertices) else 0
            our_vector = (p, self.vertices[next_i])

            if has_intersection(*vector, *our_vector):
                return [self]
        return None

    def collides_with_bounding_box(self, bounding_box: Polygon, iterations: List[int]):
        children = []
        for child in self.children:
            if nodes := child.collides_with_bounding_box(bounding_box, iterations):
                children += nodes
        if children:
            return children

        iterations[0] += 1

        min_self, max_self = self.get_limits()
        min_box, max_box = bounding_box.get_limits()

        width_self = (max_self.x - min_self.x) / 2
        height_self = (max_self.y - min_self.y) / 2

        width_box = (max_box.x - min_box.x) / 2
        height_box = (max_box.y - min_box.y) / 2

        mid_self = Point((max_self.x + min_self.x) / 2, (max_self.y + min_self.y) / 2)
        mid_box = Point((max_box.x + min_box.x) / 2, (max_box.y + min_box.y) / 2)

        if abs(mid_self.x - mid_box.x) > (width_self + width_box) or \
            abs(mid_self.y - mid_box.y) > (height_self + height_box):
            return False
        return [self]
