
from enum import Enum


Numeric = int | float
Algorithm = Enum('Algorithm', '_None BruteForce BoundingBox QuadTree')
Side = Enum('Side', 'Left Right Atop')
