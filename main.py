# ***********************************************************************************
#   PontosNoTriangulo.py
#       Autor: Márcio Sarroglia Pinho
#       pinho@pucrs.br
#   Este programa exibe um conjunto de Pontos e um triangulo em OpenGL
#   Para construir este programa, foi utilizada a biblioteca PyOpenGL, disponível em
#   http://pyopengl.sourceforge.net/documentation/index.html
#
#   Sugere-se consultar também as páginas listadas
#   a seguir:
#   http://bazaar.launchpad.net/~mcfletch/pyopengl-demo/trunk/view/head:/PyOpenGL-Demo/NeHe/lesson1.py
#   http://pyopengl.sourceforge.net/documentation/manual-3.0/index.html#GLUT
#
#   No caso de usar no MacOS, pode ser necessário alterar o arquivo ctypesloader.py,
#   conforme a descrição que está nestes links:
#   https://stackoverflow.com/questions/63475461/unable-to-import-opengl-gl-in-python-on-macos
#   https://stackoverflow.com/questions/6819661/python-location-on-mac-osx
#   Veja o arquivo Patch.rtf, armazenado na mesma pasta deste fonte.
# ***********************************************************************************

from typing import List
from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *
from src.polygon import Polygon, RecursivePolygon
from src.point import Point
from src.constants import *
import random
import sys


# Variaveis que controlam o triangulo do campo de visao
g_scenery_points = Polygon()
g_fov = Polygon()
g_base_triangle = Polygon() #? Não entendi onde é usado
g_fov_pos = Point()

g_fov_angle = 0.0

# Limites da Janela de Seleção
g_min = Point()
g_max = Point()
g_size = Point()
g_mid = Point()

g_clicked_point = Point()
g_flag_draw_axis = True

# New vars
g_algorithm = Algorithm._None
g_bounding_box = Polygon()
g_flag_draw_bounding_box = False
g_flag_draw_quadtree = False
g_flag_draw_colliding_quadtree = False
g_flag_paint_points_outside_triangle_in_red = True
g_quadtree = RecursivePolygon()
g_recreate_points = False

# Interesting maxes: 17, 71, 9, 67
g_quadtree_max_points_per_node = 20
g_quadtree_max_depth = random.randint(3, 8)

# Perf counters
g_num_green_points = 0
g_num_yellow_points = 0
g_num_red_points = 0
g_considered_vertices = 0
g_num_points_tested = 0


def calculate_intersecting_points() -> None:
    global g_algorithm
    match g_algorithm:
        case Algorithm.BruteForce:
            calculate_intersecting_points_brute_force()
        case Algorithm.BoundingBox:
            calculate_intersecting_points_bounding_box()
        case Algorithm.QuadTree:
            calculate_intersecting_points_quadtree()
        case Algorithm._None:
            return
        case _:
            return


def calculate_intersecting_points_brute_force() -> None:
    convex_polygon_point_detection(g_scenery_points.vertices)


def convex_polygon_point_detection(vertices: List[Point]) -> None:
    '''
    Convex polygon point detection algorithm.
    '''
    global g_scenery_points, g_fov, g_flag_paint_points_outside_triangle_in_red
    global g_num_green_points, g_num_yellow_points, g_num_red_points, g_considered_vertices

    g_num_green_points = 0
    g_num_yellow_points = 0
    g_num_red_points = 0
    g_considered_vertices = len(vertices)

    # Get the FOV triangle's vectors
    vec_1 = (g_fov.vertices[0], g_fov.vertices[1])
    vec_2 = (g_fov.vertices[1], g_fov.vertices[2])
    vec_3 = (g_fov.vertices[2], g_fov.vertices[0])

    glPointSize(2)

    if g_flag_paint_points_outside_triangle_in_red:
        glBegin(GL_POINTS)
        glColor3f(1, 0, 0)
        for p in g_scenery_points.vertices:
            glVertex3f(p.x, p.y, p.z)
        glEnd()

    glBegin(GL_POINTS) #? Mais eficiente fora do `for` ou dentro?
    for point in vertices:
        side_1 = Polygon.cross_product(vec_1, point)
        #? Possível otimização: verificar se `side_1 == 0` antes de computar `side_2`
        side_2 = Polygon.cross_product(vec_2, point)
        #? Possível otimização: verificar se `side_2 == 0` antes de computar `side_3`
        #? Possível otimização: verificar se `side_1 != side_2` antes de computar `side_3`
        side_3 = Polygon.cross_product(vec_3, point)
        if side_1 == side_2 == side_3: # In triangle!
            # glBegin(GL_POINTS)
            glColor3f(0, 1, 0)
            glVertex3f(point.x, point.y, point.z)
            # glEnd()
            g_num_green_points += 1
        else: # Not in triangle!
            # glBegin(GL_POINTS)
            glColor3f(1, 1, 0)
            glVertex3f(point.x, point.y, point.z)
            # glEnd()
            g_num_yellow_points += 1
    glEnd() #? Mais eficiente fora do `for` ou dentro?
    glPointSize(1)
    g_num_red_points = len(g_scenery_points.vertices) - g_num_green_points - g_num_yellow_points


def calculate_intersecting_points_bounding_box() -> None:
    global g_bounding_box, g_scenery_points, g_num_points_tested

    g_num_points_tested = 0
    convex_polygon_point_detection(get_scenery_points_in_polygon(g_bounding_box))


def calculate_intersecting_points_quadtree() -> None:
    global g_flag_draw_colliding_quadtree, g_num_points_tested
    
    iterations = [0]
    vertices: List[Point] = []
    for child in g_quadtree.children:
        if nodes := child.collides_with_bounding_box(g_bounding_box, iterations):
            for node in nodes:
                points = get_scenery_points_in_polygon(node)
                vertices += points
                g_num_points_tested += 1
                if g_flag_draw_colliding_quadtree:
                    node.draw()

    convex_polygon_point_detection(vertices)
    g_num_points_tested = iterations[0]


def get_scenery_points_in_polygon(poly: Polygon) -> List[Point]:
    global g_num_points_tested

    _min, _max = poly.get_limits()
    vertices: List[Point] = []
    for point in g_scenery_points.vertices:  # Unsorted list of points
        # Verify if `point` is inside the bounding box
        if (_min.x <= point.x <= _max.x) and (_min.y <= point.y <= _max.y):
            vertices.append(point)
        g_num_points_tested += 1
    return vertices


def generate_points(quantity: int, min: Point, max: Point):
    global g_scenery_points
    scale = Point()
    scale = (max - min) * (1.0/1000.0)

    for _ in range(quantity):
        x = random.randint(0, 1000)
        y = random.randint(0, 1000)
        x = x * scale.x + min.x
        y = y * scale.y + min.y
        p = Point(x, y)
        g_scenery_points.insert_vertex(p.x, p.y, p.z)


def create_pov_triangle():
    '''
    Cria um triangulo a partir do vetor (1,0,0), girando este vetor
    em 45 e -45 graus.

    Este vetor fica armazenado nas variáveis "TrianguloBase" e
    "CampoDeVisao".
    '''
    global g_base_triangle, g_fov

    vector = Point(1, 0, 0)

    g_base_triangle.insert_vertex(0, 0, 0)
    g_fov.insert_vertex(0, 0, 0)

    vector.rotate_z(45)
    g_base_triangle.insert_vertex(vector.x, vector.y, vector.z)
    g_fov.insert_vertex(vector.x, vector.y, vector.z)

    vector.rotate_z(-90)
    g_base_triangle.insert_vertex(vector.x, vector.y, vector.z)
    g_fov.insert_vertex(vector.x, vector.y, vector.z)


def reposition_pov_triangle():
    '''
    Posiciona o campo de visão na posicao PosicaoDoCampoDeVisao,
    com a orientacao "AnguloDoCampoDeVisao".

    O tamanho do campo de visão é de 25% da largura da janela.
    '''
    global g_size, g_fov, g_fov_pos, g_base_triangle, g_fov_angle

    _size = g_size.x * 0.25
    temp = Point()
    for i in range(g_base_triangle.get_vertices_amount()):
        temp = g_base_triangle.get_vertex(i)
        temp.rotate_z(g_fov_angle)
        g_fov.modify_edge(i, g_fov_pos + temp * _size)


def create_bounding_box() -> None:
    '''
    Creates the axis-aligned bounding box.
    '''
    global g_bounding_box

    g_bounding_box.insert_vertex(0, 0, 0) # A
    g_bounding_box.insert_vertex(1, 0, 0) # B
    g_bounding_box.insert_vertex(0, 1, 0) # C
    g_bounding_box.insert_vertex(1, 1, 0) # D


def reposition_bounding_box() -> None:
    '''
    Repositions the bounding box surrounding the FOV triangle.

    The AABB follows the following formula:

    C: (min x, max y) +-----------------+ D: (max x, max y)
                      |                 |
                      |                 |
                      |                 |
                      |                 |
    A: (min x, min y) +-----------------+ B: (max x, min y)

    '''
    global g_fov, g_bounding_box

    _min, _max = g_fov.get_limits()

    # Modify edges in a counter-clockwise motion
    g_bounding_box.modify_edge(0, Point(_min.x, _min.y, 0))
    g_bounding_box.modify_edge(1, Point(_max.x, _min.y, 0))
    g_bounding_box.modify_edge(2, Point(_max.x, _max.y, 0))
    g_bounding_box.modify_edge(3, Point(_min.x, _max.y, 0))


def create_global_quadtree() -> None:
    '''
    Creates the quadtree.
    '''
    global g_quadtree, g_min, g_max
    
    g_quadtree = RecursivePolygon()

    g_quadtree.insert_vertex(g_min.x, g_min.y, 0)
    g_quadtree.insert_vertex(g_min.x, g_max.y, 0)
    g_quadtree.insert_vertex(g_max.x, g_max.y, 0)
    g_quadtree.insert_vertex(g_max.x, g_min.y, 0)
    
    create_quadtree(g_quadtree)


def create_quadtree(parent: RecursivePolygon, depth: int = g_quadtree_max_depth) -> None:
    if depth == 0:
        return
    depth -= 1

    points = get_scenery_points_in_polygon(parent)
    if len(points) < g_quadtree_max_points_per_node:
        return

    parent.subdivide()
    for child in parent.children:
        create_quadtree(child, depth)


def fov_forward(distance):
    global g_fov_pos, g_fov_angle
    vector = Point(1, 0, 0)
    vector.rotate_z(g_fov_angle)
    g_fov_pos = g_fov_pos + vector * distance


def init():
    global g_fov_pos, g_fov_angle

    # Define a cor do fundo da tela (AZUL)
    glClearColor(0.1, 0.1, 0.1, 1)
    global g_min, g_max, g_mid, g_size

    #* Geração de cenário
    # generate_points(1000, Point(0, 0), Point(500, 500))
    # g_min, g_max = g_scenery_points.get_limits()
    
    g_min, g_max = g_scenery_points.read_points_from_file('Example/CenarioDeTeste.txt')
    # g_min, g_max = g_scenery_points.read_points_from_file('Example/CenarioGrande.txt')
    #*

    g_mid = (g_max + g_min) * 0.5  # Ponto central da janela
    g_size = (g_max - g_min)  # Tamanho da janela em X,Y

    # Ajusta variaveis do triangulo que representa o campo de visao
    g_fov_pos = g_mid
    g_fov_angle = 0

    # Cria o triangulo que representa o campo de visao
    create_pov_triangle()
    reposition_pov_triangle()

    # Creates the bounding box
    create_bounding_box()
    reposition_bounding_box()

    # Creates the quadtree
    create_global_quadtree()

    # Calculates the intersecting points according to the desired algorithm
    calculate_intersecting_points()


def draw_line(P1, P2):
    glBegin(GL_LINES)
    glVertex3f(P1.x, P1.y, P1.z)
    glVertex3f(P2.x, P2.y, P2.z)
    glEnd()


def draw_axis():
    global g_min, g_max, g_mid

    glBegin(GL_LINES)
    # eixo horizontal
    glVertex2f(g_min.x, g_mid.y)
    glVertex2f(g_max.x, g_mid.y)
    # eixo vertical
    glVertex2f(g_mid.x, g_min.y)
    glVertex2f(g_mid.x, g_max.y)
    glEnd()


def reshape(w, h):
    global g_min, g_max

    glViewport(0, 0, w, h)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    # Cria uma folga na Janela de Selecão, com 10% das dimensoes do poligono
    x_border = abs(g_max.x-g_min.x)*0.1
    y_border = abs(g_max.y-g_min.y)*0.1
    glOrtho(g_min.x-x_border, g_max.x+x_border,
            g_min.y-y_border, g_max.y+y_border, 0.0, 1.0)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()


def display():
    global g_clicked_point, g_flag_draw_axis, g_recreate_points, g_min, g_max, g_scenery_points

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)  # type: ignore
    glLoadIdentity()
    glPointSize(1)
    glColor3f(1.0, 1.0, 0.0)

    if (g_flag_draw_axis):
        glLineWidth(1)
        glColor3f(1, 1, 1)  # R, G, B  [0..1]
        draw_axis()

    glColor3f(1, 1, 0)  # R, G, B  [0..1]
    
    if g_recreate_points:
        g_recreate_points = not g_recreate_points
        g_scenery_points = Polygon()
        generate_points(1000, Point(0, 0), Point(500, 500))
        g_min, g_max = g_scenery_points.get_limits()
        create_global_quadtree()

    g_scenery_points.draw_vertices()

    glPointSize(5)
    glLineWidth(3)
    glColor3f(1, 0, 0)  # R, G, B  [0..1]
    g_fov.draw()

    if g_flag_draw_bounding_box:
        glLineWidth(1)
        glColor3f(0.5, 1, 0.5)
        g_bounding_box.draw()

    if g_flag_draw_quadtree:
        glLineWidth(0.5)
        g_quadtree.draw()

    calculate_intersecting_points()

    glutSwapBuffers()


def keyboard(*args):
    '''
    The function called whenever a key is pressed.

    Note the use of Python tuples to pass in: (key, x, y)
    '''

    global g_flag_draw_axis, g_fov, g_algorithm, g_flag_draw_bounding_box, g_flag_draw_quadtree, g_flag_paint_points_outside_triangle_in_red, g_fov_angle, g_quadtree_max_points_per_node, g_recreate_points
    global g_num_green_points, g_num_yellow_points, g_num_red_points, g_considered_vertices, g_flag_draw_colliding_quadtree, g_fov_pos, g_mid, g_num_points_tested

    match args[0]:
        case b'q' | b'\x1b':
            os._exit(0)
        case b'p':
            g_scenery_points.print_vertices()
        case b'1':
            p1, p2 = g_scenery_points.get_edge(0)
            p1.print()
            p2.print()
        case b' ':
            g_flag_draw_axis = not g_flag_draw_axis
        case b'+': #TODO Aumentar FOV
            _size = g_size.x * 0.25
            temp = g_base_triangle.get_vertex(1)
            temp.rotate_z(g_fov_angle + 2)
            g_fov.modify_edge(1, g_fov_pos + temp * _size)
            g_base_triangle.modify_edge(1, g_base_triangle.get_vertex(1) + temp * _size)
            temp = g_base_triangle.get_vertex(2)
            temp.rotate_z(g_fov_angle - 2)
            g_fov.modify_edge(2, g_fov_pos + temp * _size)
            g_base_triangle.modify_edge(2, g_base_triangle.get_vertex(2) + temp * _size)
        case b'-': #TODO Diminuir FOV
            _size = g_size.x * 0.25
            temp = g_base_triangle.get_vertex(1)
            temp.rotate_z(g_fov_angle - 2)
            g_fov.modify_edge(1, g_fov_pos + temp * _size)
            g_base_triangle.modify_edge(1, g_base_triangle.get_vertex(1) + temp * _size)
            temp = g_base_triangle.get_vertex(2)
            temp.rotate_z(g_fov_angle + 2)
            g_fov.modify_edge(2, g_fov_pos + temp * _size)
            g_base_triangle.modify_edge(2, g_base_triangle.get_vertex(2) + temp * _size)
        case b'!': # Shift + 1
            print('Setting collision algorithm to "_None"')
            g_algorithm = Algorithm._None
        case b'@': # Shift + 2
            print('Setting collision algorithm to "BruteForce"')
            g_algorithm = Algorithm.BruteForce
        case b'#': # Shift + 3
            print('Setting collision algorithm to "BoundingBox"')
            g_algorithm = Algorithm.BoundingBox
        case b'$': # Shift + 4
            print('Setting collision algorithm to "QuadTree"')
            g_algorithm = Algorithm.QuadTree
        case b'b':
            g_flag_draw_bounding_box = not g_flag_draw_bounding_box
        case b't':
            g_flag_draw_quadtree = not g_flag_draw_quadtree
        case b'c':
            g_flag_paint_points_outside_triangle_in_red = not g_flag_paint_points_outside_triangle_in_red
        case b's':
            try:
                n_points = int(input('Max number of points in a quadtree node? '))
                g_quadtree_max_points_per_node = n_points
                create_global_quadtree()
            except:
                pass
        case b'r':
            g_recreate_points = True
        case b'P':
            print(f'Pontos dentro do triângulo: {g_num_green_points}\n' + \
                  f'Pontos considerados (total): {g_num_green_points + g_num_yellow_points}\n' + \
                  f'Pontos considerados (miss): {g_num_yellow_points}\n' + \
                  f'Pontos ignorados: {g_num_red_points}\n' + \
                  f'Pontos totais: {g_considered_vertices}\n' + \
                  f'g_num_points_tested: {g_num_points_tested}')
        case b'T':
            g_flag_draw_colliding_quadtree = not g_flag_draw_colliding_quadtree
        case b'O': # PosicionaCampoDeVisao
            try:
                match int(input('Posição? Apenas números de 1 a 4.\n> ')):
                    case 1:
                        g_fov_angle = 0.0
                        g_fov_pos = g_mid
                    case 2:
                        g_fov_angle = 90.0
                        g_fov_pos = g_mid
                    case 3:
                        g_fov_angle = 90.0
                        g_fov_pos = g_mid * 0.5
                    case 4:
                        g_fov_angle = 0.0
                        g_fov_pos = g_mid + (g_mid * 0.5)

                reposition_pov_triangle()
                reposition_bounding_box()
            except:
                print('Erro')

    # Forca o redesenho da tela
    glutPostRedisplay()


def arrow_keys(a_keys: int, x: int, y: int):
    global g_fov_angle, g_base_triangle

    #print ("Tecla:", a_keys)
    if a_keys == GLUT_KEY_UP:         # Se pressionar UP
        fov_forward(2)
    if a_keys == GLUT_KEY_DOWN:       # Se pressionar DOWN
        fov_forward(-2)
    if a_keys == GLUT_KEY_LEFT:       # Se pressionar LEFT
        g_fov_angle = g_fov_angle + 2
    if a_keys == GLUT_KEY_RIGHT:      # Se pressionar RIGHT
        g_fov_angle = g_fov_angle - 2

    reposition_pov_triangle()
    reposition_bounding_box()

    glutPostRedisplay()


def mouse(button: int, state: int, x: int, y: int):
    global g_clicked_point
    if (state != GLUT_DOWN):
        return
    if (button != GLUT_RIGHT_BUTTON):
        return
    #print ("Mouse:", x, ",", y)
    # Converte a coordenada de tela para o sistema de coordenadas do
    # universo definido pela glOrtho
    vport = glGetIntegerv(GL_VIEWPORT)
    mvmatrix = glGetDoublev(GL_MODELVIEW_MATRIX)
    projmatrix = glGetDoublev(GL_PROJECTION_MATRIX)
    realY = vport[3] - y
    worldCoordinate1 = gluUnProject(x, realY, 0, mvmatrix, projmatrix, vport)

    g_clicked_point = Point(
        worldCoordinate1[0], worldCoordinate1[1], worldCoordinate1[2])
    g_clicked_point.print("Ponto Clicado:")

    glutPostRedisplay()


def mouseMove(x: int, y: int):
    # glutPostRedisplay()
    return


if __name__ == '__main__':
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA) # type: ignore
    # Define o tamanho inicial da janela grafica do programa
    glutInitWindowSize(500, 500)
    glutInitWindowPosition(100, 100)
    wind = glutCreateWindow("Pontos no Triangulo")
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    # glEnable(GL_BLEND)
    glutDisplayFunc(display)
    # glutIdleFunc(display)
    glutReshapeFunc(reshape)
    glutKeyboardFunc(keyboard)
    glutSpecialFunc(arrow_keys)
    glutMouseFunc(mouse)
    init()

    try:
        glutMainLoop()
    except SystemExit:
        pass
