# T1 - Computação Gráfica - CC 2022/2 PUCRS

[Este repositório](https://gitlab.com/debemdeboas/pucrs-computacao-grafica-t1) contém o código
referente ao primeiro trabalho da cadeira de Computação Gráfica do curso de Ciência de Computação
da PUCRS no semestre 2022/2.
O trabalho foi desenvolvido em Python 3.10 em Windows 64-bit utilizando os pacotes não-oficiais
do `PyOpenGL` e `PyOpenGL_Accelerate`. Os arquivos `.whl` estão armazenados na pasta [`lib`](./lib).

Também foi montado um relatório com os dados gerados pelo programa. Ele se encontra [nesse arquivo](./relatorio.md).
O vídeo gravado está [no YouTube](https://youtu.be/U2GXVwzQwKE) e [localmente](./doc/relatorio.mkv).

## Autores

Isabella Do Prado Pagnoncelli, Rafael Almeida de Bem.

## Instalação dos pacotes

```commandline
$ python3.10 -m venv venv
$ ./venv/Scripts/activate
(venv) $ pip install --no-index --find-links=lib/ -r requirements.txt
```

## O programa

Para rodar o programa basta rodar o arquivo `main.py`.
Esse projeto dispõe de diversos atalhos de teclado para interagir com o sistema.
Os atalhos para seleção de algoritmos já foram demonstrados [aqui](#algoritmos-de-detecção-de-colisão).
O restante dos atalhos está na tabela a seguir:

|             Atalho              |                         Utilidade                          |
| :-----------------------------: | :--------------------------------------------------------: |
|          <kbd>b</kbd>           |                   Desenha a bounding box                   |
|          <kbd>c</kbd>           |             Pinta pontos ignorados de vermelho             |
|          <kbd>O</kbd>           |        Configura a posição do objeto (via terminal)        |
|          <kbd>P</kbd>           |      **Ver entrada <kbd>Shift</kbd> + <kbd>p</kbd>**       |
|          <kbd>q</kbd>           |                   Quit, sai do programa                    |
|          <kbd>r</kbd>           |  Recria o cenário com novos pontos (e uma nova quadtree)   |
|          <kbd>s</kbd>           | Configura o número máximo de pontos em um nodo da quadtree |
|          <kbd>t</kbd>           |              Desenha a quadtree e seus nodos               |
|          <kbd>T</kbd>           |   Desenha os nodos da quadtree que colidem com o objeto    |
|        <kbd>Space</kbd>         |      Liga/desliga a exibição dos eixos de coordenadas      |
| <kbd>Shift</kbd> + <kbd>p</kbd> |              Imprime os dados de performance               |

### Algoritmos de detecção de colisão

Por padrão o programa não utiliza nenhum algoritmo.
Precisamos indicar ao programa qual algoritmo usar.
Isso é feito da seguinte forma:

|  Algoritmo   |            Atalho             |
| :----------: | :---------------------------: |
|    Nenhum    | <kbd>Shift</kbd>+<kbd>1</kbd> |
| Brute-force  | <kbd>Shift</kbd>+<kbd>2</kbd> |
| Bounding box | <kbd>Shift</kbd>+<kbd>3</kbd> |
|   Quadtree   | <kbd>Shift</kbd>+<kbd>4</kbd> |

#### Brute-force (força bruta)

Esse algoritmo considera todos os pontos do cenário para calcular as colisões.

![Execução do algoritmo força bruta](doc/brute-force.gif)

#### Axis aligned bounding box

Para visualizar a bounding box pode-se usar o atalho <kbd>b</kbd>.

![Execução do algoritmo força bruta](doc/aabb.gif)

#### Quadtree

Para visualizar a quadtree devemos usar o atalho <kbd>t</kbd>.
Também demonstrei nesse GIF a funcionalidade re-gerar o cenário,
apertando <kbd>r</kbd>.

![Execução do algoritmo força bruta](doc/quadtree.gif)
