# Relatório de posições

Foi gravado um vídeo demonstrando a geração desses dados.
O vídeo está [no YouTube](https://youtu.be/yeID3wrRF0E).

Os campos considerados dos logs foram:

- `Pontos considerados (total)` como a coluna "Testes força bruta"
- `g_num_points_tested` como a coluna "Testes de envelope"

## `CenarioDeTeste.txt`

### Posição 1

|     Modo     | Testes força bruta | Testes de envelope |
| :----------: | :----------------: | :----------------: |
| Força Bruta  |        $80$        |        $2000$      |
| Bounding Box |        $6$         |        $80$        |
|   Quadtree   |        $33$        |        $20$        |

### Posição 2

|     Modo     | Testes força bruta | Testes de envelope |
| :----------: | :----------------: | :----------------: |
| Força Bruta  |        $80$        |        $20$        |
| Bounding Box |        $6$         |        $80$        |
|   Quadtree   |        $30$        |        $20$        |

### Posição 3

|     Modo     | Testes força bruta | Testes de envelope |
| :----------: | :----------------: | :----------------: |
| Força Bruta  |        $80$        |        $20$        |
| Bounding Box |        $6$         |        $80$        |
|   Quadtree   |        $20$        |        $22$        |

### Posição 4

|     Modo     | Testes força bruta | Testes de envelope |
| :----------: | :----------------: | :----------------: |
| Força Bruta  |        $80$        |        $22$        |
| Bounding Box |        $6$         |        $80$        |
|   Quadtree   |        $34$        |        $21$        |

<details><summary>Logs</summary>

```
Posição? Apenas números de 1 a 4.
> 1 
Setting collision algorithm to "_None"
Pontos dentro do triângulo: 0
Pontos considerados (total): 0
Pontos considerados (miss): 0
Pontos ignorados: 0
Pontos totais: 0
g_num_points_tested: 2000
Setting collision algorithm to "BruteForce"
Pontos dentro do triângulo: 4
Pontos considerados (total): 80
Pontos considerados (miss): 76
Pontos ignorados: 0
Pontos totais: 80
g_num_points_tested: 2000
Setting collision algorithm to "BoundingBox"
Pontos dentro do triângulo: 4
Pontos considerados (total): 6
Pontos considerados (miss): 2
Pontos ignorados: 74
Pontos totais: 6
g_num_points_tested: 80
Setting collision algorithm to "QuadTree"
Pontos dentro do triângulo: 4
Pontos considerados (total): 33
Pontos considerados (miss): 29
Pontos ignorados: 47
Pontos totais: 33
g_num_points_tested: 20
Posição? Apenas números de 1 a 4.
> 2
Setting collision algorithm to "BruteForce"
Pontos dentro do triângulo: 2
Pontos considerados (total): 80
Pontos considerados (miss): 78
Pontos ignorados: 0
Pontos totais: 80
g_num_points_tested: 20
Setting collision algorithm to "BoundingBox"
Pontos dentro do triângulo: 2
Pontos considerados (total): 6
Pontos considerados (miss): 4
Pontos ignorados: 74
Pontos totais: 6
g_num_points_tested: 80
Setting collision algorithm to "QuadTree"
Pontos dentro do triângulo: 2
Pontos considerados (total): 30
Pontos considerados (miss): 28
Pontos ignorados: 50
Pontos totais: 30
g_num_points_tested: 20
Posição? Apenas números de 1 a 4.
> 3 
Setting collision algorithm to "BruteForce"
Pontos dentro do triângulo: 2
Pontos considerados (total): 80
Pontos considerados (miss): 78
Pontos ignorados: 0
Pontos totais: 80
g_num_points_tested: 20
Setting collision algorithm to "BoundingBox"
Pontos dentro do triângulo: 2
Pontos considerados (total): 6
Pontos considerados (miss): 4
Pontos ignorados: 74
Pontos totais: 6
g_num_points_tested: 80
Setting collision algorithm to "QuadTree"
Pontos dentro do triângulo: 2
Pontos considerados (total): 20
Pontos considerados (miss): 18
Pontos ignorados: 60
Pontos totais: 20
g_num_points_tested: 22
Posição? Apenas números de 1 a 4.
> 4
Setting collision algorithm to "BruteForce"
Pontos dentro do triângulo: 4
Pontos considerados (total): 80
Pontos considerados (miss): 76
Pontos ignorados: 0
Pontos totais: 80
g_num_points_tested: 22
Setting collision algorithm to "BoundingBox"
Pontos dentro do triângulo: 4
Pontos considerados (total): 6
Pontos considerados (miss): 2
Pontos ignorados: 74
Pontos totais: 6
g_num_points_tested: 80
Setting collision algorithm to "QuadTree"
Pontos dentro do triângulo: 4
Pontos considerados (total): 34
Pontos considerados (miss): 30
Pontos ignorados: 46
Pontos totais: 34
g_num_points_tested: 21

```

</details>
